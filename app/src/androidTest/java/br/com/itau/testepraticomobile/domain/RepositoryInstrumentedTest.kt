package br.com.itau.testepraticomobile.domain

import android.os.Parcel
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
@RunWith(AndroidJUnit4::class)
class RepositoryInstrumentedTest {
    private lateinit var repository: Repository

    private val ID = 1L
    private val NAME = "NAME"
    private val DESCRIPTION = "DESCRIPTION"
    private val STARGAZERS_COUNT = 100
    private val FORKS_COUNT = 200

    private val OWNER_ID = 10L
    private val OWNER_LOGIN = "OWNER_LOGIN"
    private val OWNER_AVATAR_URL = "OWNER_AVATAR_URL"

    private val owner: Owner = Owner(OWNER_ID, OWNER_LOGIN, OWNER_AVATAR_URL)

    @Before
    fun setUp() {
        this.repository =
            Repository(ID, NAME, DESCRIPTION, STARGAZERS_COUNT, FORKS_COUNT, this.owner)
    }

    @Test
    fun Verify_parcelable_write_and_read_is_valid() {
        val parcel = Parcel.obtain()
        this.repository.apply {
            writeToParcel(parcel, describeContents())
        }

        parcel.setDataPosition(0)

        val repositoryParcel: Repository = Repository.CREATOR.createFromParcel(parcel)

        assertEquals(this.repository.id, repositoryParcel.id)
        assertEquals(this.repository.name, repositoryParcel.name)
        assertEquals(this.repository.description, repositoryParcel.description)
        assertEquals(this.repository.stargazers_count, repositoryParcel.stargazers_count)
        assertEquals(this.repository.forks_count, repositoryParcel.forks_count)
        assertEquals(this.repository.owner.toString(), repositoryParcel.owner.toString())
        assertEquals(this.repository.toString(), repositoryParcel.toString())
    }
}