package br.com.itau.testepraticomobile.domain

import android.os.Parcel
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
@RunWith(AndroidJUnit4::class)
class OwnerInstrumentedTest {
    private lateinit var owner: Owner

    private val ID = 1L
    private val LOGIN = "LOGIN"
    private val AVATAR_URL = "AVATAR_URL"

    @Before
    fun setUp() {
        this.owner = Owner(ID, LOGIN, AVATAR_URL)
    }

    @Test
    fun Verify_parcelable_write_and_read_is_valid() {
        val parcel = Parcel.obtain()
        this.owner.apply {
            writeToParcel(parcel, describeContents())
        }

        parcel.setDataPosition(0)

        val ownerParcel: Owner = Owner.CREATOR.createFromParcel(parcel)

        assertEquals(owner.id, ownerParcel.id)
        assertEquals(owner.login, ownerParcel.login)
        assertEquals(owner.avatar_url, ownerParcel.avatar_url)
        assertEquals(owner.toString(), ownerParcel.toString())
    }
}