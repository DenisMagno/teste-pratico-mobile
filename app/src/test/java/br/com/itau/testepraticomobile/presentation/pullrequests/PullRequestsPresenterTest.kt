package br.com.itau.testepraticomobile.presentation.pullrequests

import android.content.Context
import br.com.itau.testepraticomobile.data.providers.GitHubProvider
import br.com.itau.testepraticomobile.di.injectDataModule
import br.com.itau.testepraticomobile.domain.Owner
import br.com.itau.testepraticomobile.domain.Repository
import org.junit.Before
import org.junit.Rule

import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class PullRequestsPresenterTest : AutoCloseKoinTest() {
    @Mock
    private lateinit var context: Context

    @Mock
    private lateinit var view: PullRequestsContract.View

    @Mock
    private lateinit var owner: Owner

    @Mock
    private lateinit var repository: Repository

    private lateinit var gitHubProvider: GitHubProvider

    @Mock
    private lateinit var listPullRequestsCallback: GitHubProvider.ExpectedListPullRequests

    @get:Rule
    val mockProvider = MockProviderRule.create { _ ->
        mock(GitHubProvider::class.java)
    }

    private val ownerName = "ownerName"
    private val repoName = "repoName"

    private lateinit var pullRequestsPresenter: PullRequestsPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        startKoin {
            androidContext(context)
            module {
                injectDataModule()
            }
        }

        this.gitHubProvider = declareMock {
            given(
                this.buscarPullRequests(
                    listPullRequestsCallback,
                    ownerName,
                    repoName
                )
            ).willCallRealMethod()
        }

        this.pullRequestsPresenter = PullRequestsPresenter()

        `when`(this.repository.owner).thenReturn(this.owner)
        `when`(this.owner.login).thenReturn("login")
        `when`(this.repository.name).thenReturn("name")
    }

    @Test
    fun `Verify getPullRequests invoke method showLoading in View`() {
        this.pullRequestsPresenter.onAttach(this.view, this.repository)

        this.pullRequestsPresenter.getPullRequests()

        verify(this.view).showLoading()
    }
}