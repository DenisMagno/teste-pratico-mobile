package br.com.itau.testepraticomobile.domain

import org.junit.Assert.*
import org.junit.Test

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class UserTest {
    private val ID = 1L
    private val LOGIN = "USER_LOGIN"
    private val AVATAR_URL = "USER_AVATAR_URL"

    private val TO_STRING =
        "User(id=$ID, login='$LOGIN', avatar_url='$AVATAR_URL')"

    @Test
    fun `Assert constructor is valid`() {
        val user = User(ID, LOGIN, AVATAR_URL)

        assertEquals(ID, user.id)
        assertEquals(LOGIN, user.login)
        assertEquals(AVATAR_URL, user.avatar_url)
    }

    @Test
    fun `Assert toString is valid`() {
        val user = User(ID, LOGIN, AVATAR_URL)

        assertEquals(TO_STRING, user.toString())
    }
}