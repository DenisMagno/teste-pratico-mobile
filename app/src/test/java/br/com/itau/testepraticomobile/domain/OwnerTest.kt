package br.com.itau.testepraticomobile.domain

import org.junit.Assert.*
import org.junit.Test

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class OwnerTest {
    private val ID = 1L
    private val LOGIN = "LOGIN"
    private val AVATAR_URL = "AVATAR_URL"

    private val TO_STRING = "Owner(id=$ID, login='$LOGIN', avatar_url='$AVATAR_URL')"

    @Test
    fun `Assert constructor is valid`() {
        val owner = Owner(ID, LOGIN, AVATAR_URL)

        assertEquals(ID, owner.id)
        assertEquals(LOGIN, owner.login)
        assertEquals(AVATAR_URL, owner.avatar_url)
    }

    @Test
    fun `Assert toString is valid`() {
        val owner = Owner(ID, LOGIN, AVATAR_URL)

        assertEquals(TO_STRING, owner.toString())
    }

    @Test
    fun `Assert describeContents is valid`() {
        val owner = Owner(ID, LOGIN, AVATAR_URL)

        assertEquals(0, owner.describeContents())
    }
}