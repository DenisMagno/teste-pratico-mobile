package br.com.itau.testepraticomobile.presentation.repositories

import android.content.Context
import br.com.itau.testepraticomobile.data.providers.GitHubProvider
import br.com.itau.testepraticomobile.di.injectDataModule
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class RepositoriesPresenterTest : AutoCloseKoinTest() {
    @Mock
    private lateinit var context: Context

    @Mock
    private lateinit var view: RepositoriesContract.View

    private lateinit var gitHubProvider: GitHubProvider

    @Mock
    private lateinit var listRepositoriesCallback: GitHubProvider.ExpectedListRepositories

    @get:Rule
    val mockProvider = MockProviderRule.create { _ ->
        mock(GitHubProvider::class.java)
    }

    private val language = "language:Java"
    private val sort = "stars"
    private val page = 1

    private lateinit var repositoriesPresenter: RepositoriesPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        startKoin {
            androidContext(context)
            module {
                injectDataModule()
            }
        }

        this.gitHubProvider = declareMock {
            given(
                this.buscarRepositorios(
                    listRepositoriesCallback,
                    language,
                    sort,
                    page
                )
            ).willCallRealMethod()
        }

        this.repositoriesPresenter = RepositoriesPresenter()
    }

    @Test
    fun `Verify getRepositories invoke method showLoading in View`() {
        this.repositoriesPresenter.onAttach(this.view)

        this.repositoriesPresenter.getRepositories()

        verify(this.view).showLoading()
    }
}