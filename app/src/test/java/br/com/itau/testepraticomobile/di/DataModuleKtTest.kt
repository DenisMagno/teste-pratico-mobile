package br.com.itau.testepraticomobile.di

import android.content.Context
import br.com.itau.testepraticomobile.data.RetrofitInitializer
import br.com.itau.testepraticomobile.data.providers.GitHubProvider
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.test.AutoCloseKoinTest
import org.koin.test.get
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class DataModuleKtTest : AutoCloseKoinTest() {
    @Mock
    private lateinit var context: Context

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        startKoin {
            androidContext(context)
            modules(arrayListOf(injectDataModule()))
        }

    }

    @Test
    fun `Assert RetrofitInitializer is valid`() {
        val retrofitInitializer: RetrofitInitializer = get()

        assertNotNull(retrofitInitializer)
    }

    @Test
    fun `Assert GitHubProvider is valid`() {
        val gitHubProvider: GitHubProvider = get()

        assertNotNull(gitHubProvider)
    }
}