package br.com.itau.testepraticomobile.di

import android.content.Context
import br.com.itau.testepraticomobile.presentation.pullrequests.PullRequestsPresenter
import br.com.itau.testepraticomobile.presentation.repositories.RepositoriesPresenter
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.test.AutoCloseKoinTest
import org.koin.test.get
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class PresentationModuleKtTest : AutoCloseKoinTest() {
    @Mock
    private lateinit var context: Context

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        startKoin {
            androidContext(context)
            modules(arrayListOf(injectPresentationModule()))
        }

    }

    @Test
    fun `Assert RepositoriesPresenter is valid`() {
        val repositoriesPresenter: RepositoriesPresenter = get()

        assertNotNull(repositoriesPresenter)
    }

    @Test
    fun `Assert PullRequestsPresenter is valid`() {
        val pullRequestsPresenter: PullRequestsPresenter = get()

        assertNotNull(pullRequestsPresenter)
    }
}