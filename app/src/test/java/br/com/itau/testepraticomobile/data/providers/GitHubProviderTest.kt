package br.com.itau.testepraticomobile.data.providers

import android.content.Context
import br.com.itau.testepraticomobile.data.RetrofitInitializer
import br.com.itau.testepraticomobile.data.resources.GitHubResource
import br.com.itau.testepraticomobile.data.responses.BaseResponse
import br.com.itau.testepraticomobile.di.injectDataModule
import br.com.itau.testepraticomobile.domain.PullRequest
import br.com.itau.testepraticomobile.domain.Repository
import org.junit.Before

import org.junit.Rule
import org.junit.Test
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.test.AutoCloseKoinTest
import org.koin.test.inject
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import retrofit2.Call

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class GitHubProviderTest : AutoCloseKoinTest() {
    @Mock
    private lateinit var context: Context

    @Mock
    private lateinit var callbackListRepositories: GitHubProvider.ExpectedListRepositories

    @Mock
    private lateinit var callBuscarRepositorios: Call<BaseResponse<Repository>>

    @Mock
    private lateinit var callbackListPullRequests: GitHubProvider.ExpectedListPullRequests

    @Mock
    private lateinit var callBuscarPullRequests: Call<ArrayList<PullRequest>>

    @Mock
    private lateinit var gitHubResource: GitHubResource

    @get:Rule
    val mockProvider = MockProviderRule.create { _ ->
        mock(RetrofitInitializer::class.java)
    }

    private val language: String = "language:Java"

    private val sort: String = "stars"

    private val page: Int = 1

    private val ownerName: String = "ownerName"

    private val repoName: String = "repoName"

    private val gitHubProvider: GitHubProvider by inject()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        startKoin {
            androidContext(context)
            modules(arrayListOf(injectDataModule()))
        }

        declareMock<RetrofitInitializer> {
            given(this.getGitHubResource()).willReturn(gitHubResource)
        }

        `when`(
            this.gitHubResource.listRepositories(
                this.language,
                this.sort,
                this.page
            )
        ).thenReturn(this.callBuscarRepositorios)

        `when`(
            this.gitHubResource.listPullRequests(
                this.ownerName,
                this.repoName,
            )
        ).thenReturn(this.callBuscarPullRequests)
    }

    @Test
    fun `Verify buscarRepositorios invoke method listRepositories in Resource`() {
        this.gitHubProvider.buscarRepositorios(
            this.callbackListRepositories,
            this.language,
            this.sort,
            this.page
        )

        verify(this.gitHubResource).listRepositories(this.language, this.sort, this.page)
    }

    @Test
    fun `Verify buscarPullRequests invoke method listPullRequests in Resource`() {
        this.gitHubProvider.buscarPullRequests(
            this.callbackListPullRequests,
            this.ownerName,
            this.repoName,
        )

        verify(this.gitHubResource).listPullRequests(this.ownerName, this.repoName)
    }
}