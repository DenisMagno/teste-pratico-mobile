package br.com.itau.testepraticomobile.data.responses

import org.junit.Assert.*
import org.junit.Test

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class BaseResponseTest{
    private val TOTAL_COUNT = 1
    private val INCOMPLETE_RESULTS = false
    private val ITEMS = ArrayList<String>()

    private val TO_STRING = "BaseResponse(total_count=$TOTAL_COUNT, incomplete_results=$INCOMPLETE_RESULTS, items=$ITEMS)"

    @Test
    fun `Assert constructor is valid`() {
        val baseResponse = BaseResponse(TOTAL_COUNT, INCOMPLETE_RESULTS, ITEMS)

        assertEquals(TOTAL_COUNT, baseResponse.total_count)
        assertEquals(INCOMPLETE_RESULTS, baseResponse.incomplete_results)
        assertEquals(ITEMS, baseResponse.items)
    }

    @Test
    fun `Assert toString is valid`() {
        val baseResponse = BaseResponse(TOTAL_COUNT, INCOMPLETE_RESULTS, ITEMS)

        assertEquals(TO_STRING, baseResponse.toString())
    }
}