package br.com.itau.testepraticomobile.domain

import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class PullRequestTest {
    private val ID = 1L
    private val TITLE = "TITLE"
    private val BODY = "BODY"
    private val CREATED_AT = "CREATED_AT"

    private val USER_ID = 10L
    private val USER_LOGIN = "USER_LOGIN"
    private val USER_AVATAR_URL = "USER_AVATAR_URL"
    private val USER_TO_STRING =
        "User(id=$USER_ID, login='$USER_LOGIN', avatar_url='$USER_AVATAR_URL')"

    private val TO_STRING =
        "PullRequest(id=$ID, title='$TITLE', body='$BODY', created_at='$CREATED_AT', user=$USER_TO_STRING)"

    @Mock
    private lateinit var user: User

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `Assert constructor is valid`() {
        val pullRequest = PullRequest(ID, TITLE, BODY, CREATED_AT, this.user)

        assertEquals(ID, pullRequest.id)
        assertEquals(TITLE, pullRequest.title)
        assertEquals(BODY, pullRequest.body)
        assertEquals(CREATED_AT, pullRequest.created_at)
        assertEquals(this.user, pullRequest.user)
    }

    @Test
    fun `Assert toString is valid`() {
        `when`(this.user.toString()).thenReturn(USER_TO_STRING)

        val pullRequest = PullRequest(ID, TITLE, BODY, CREATED_AT, this.user)

        assertEquals(TO_STRING, pullRequest.toString())
    }
}