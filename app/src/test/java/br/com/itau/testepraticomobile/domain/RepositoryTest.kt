package br.com.itau.testepraticomobile.domain

import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class RepositoryTest {
    private val ID = 1L
    private val NAME = "NAME"
    private val DESCRIPTION = "DESCRIPTION"
    private val STARGAZERS_COUNT = 100
    private val FORKS_COUNT = 200

    private val OWNER_ID = 10L
    private val OWNER_LOGIN = "OWNER_LOGIN"
    private val OWNER_AVATAR_URL = "OWNER_AVATAR_URL"

    private val OWNER_TO_STRING =
        "Owner(id=$OWNER_ID, login='$OWNER_LOGIN', avatar_url='$OWNER_AVATAR_URL')"

    private val TO_STRING =
        "Repository(id=$ID, name='$NAME', description='$DESCRIPTION', stargazers_count=$STARGAZERS_COUNT, forks_count=$FORKS_COUNT, owner=$OWNER_TO_STRING)"

    @Mock
    private lateinit var owner: Owner

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `Assert constructor is valid`() {
        val repository =
            Repository(ID, NAME, DESCRIPTION, STARGAZERS_COUNT, FORKS_COUNT, this.owner)

        assertEquals(ID, repository.id)
        assertEquals(NAME, repository.name)
        assertEquals(DESCRIPTION, repository.description)
        assertEquals(STARGAZERS_COUNT, repository.stargazers_count)
        assertEquals(FORKS_COUNT, repository.forks_count)
        assertEquals(this.owner, repository.owner)
    }

    @Test
    fun `Assert toString is valid`() {
        `when`(this.owner.toString()).thenReturn(OWNER_TO_STRING)

        val repository =
            Repository(ID, NAME, DESCRIPTION, STARGAZERS_COUNT, FORKS_COUNT, this.owner)

        assertEquals(TO_STRING, repository.toString())
    }

    @Test
    fun `Assert describeContents is valid`() {
        val repository =
            Repository(ID, NAME, DESCRIPTION, STARGAZERS_COUNT, FORKS_COUNT, this.owner)

        assertEquals(0, repository.describeContents())
    }
}