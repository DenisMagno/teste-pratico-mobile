package br.com.itau.testepraticomobile.common

import org.junit.Assert.*
import org.junit.Test

/**
 * Created by Denis Magno on 06/12/2020
 * denis_magno16@hotmail.com
 */
class ConstantsTests {
    @Test
    fun `Assert BUNDLE_REPOSITORY_EXTRA_REPOSITORIES_ACTIVITY_TO_PULL_REQUEST_ACTIVITY is valid`() {
        assertEquals(
            "BUNDLE_REPOSITORY_EXTRA_REPOSITORIES_ACTIVITY_TO_PULL_REQUEST_ACTIVITY",
            Constants.BUNDLE_REPOSITORY_EXTRA_REPOSITORIES_ACTIVITY_TO_PULL_REQUEST_ACTIVITY
        )
    }

    @Test
    fun `Assert LANGUAGE_REPOSITORIES is valid`() {
        assertEquals(
            "language:Java",
            Constants.LANGUAGE_REPOSITORIES
        )
    }

    @Test
    fun `Assert SORT_REPOSITORIES is valid`() {
        assertEquals(
            "stars",
            Constants.SORT_REPOSITORIES
        )
    }
}