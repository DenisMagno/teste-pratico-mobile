package br.com.itau.testepraticomobile.data.providers.generics

import br.com.itau.testepraticomobile.data.RetrofitInitializer
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
abstract class BaseProvider : KoinComponent {
    internal val TAG: String = "CustomLog - ${this.javaClass.simpleName}"

    protected val retrofit: RetrofitInitializer by inject()
}