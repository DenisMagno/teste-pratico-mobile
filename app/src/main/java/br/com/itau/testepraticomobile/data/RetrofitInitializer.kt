package br.com.itau.testepraticomobile.data

import br.com.itau.testepraticomobile.data.resources.GitHubResource
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
class RetrofitInitializer {
    private val baseUrl = "https://api.github.com/"

    private val retrofit: Retrofit

    init {
        val client = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        this.retrofit = Retrofit.Builder()
            .baseUrl(this.baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getGitHubResource(): GitHubResource {
        return this.retrofit.create(GitHubResource::class.java)
    }
}