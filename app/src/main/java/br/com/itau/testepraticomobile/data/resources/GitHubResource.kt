package br.com.itau.testepraticomobile.data.resources

import br.com.itau.testepraticomobile.data.responses.BaseResponse
import br.com.itau.testepraticomobile.domain.PullRequest
import br.com.itau.testepraticomobile.domain.Repository
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
interface GitHubResource {
    @GET("search/repositories")
    fun listRepositories(
        @Query("q") language: String,
        @Query("sort") sort: String,
        @Query("page") page: Int
    ): Call<BaseResponse<Repository>>

    @GET("repos/{owner-name}/{repo-name}/pulls")
    fun listPullRequests(
        @Path("owner-name") ownerName: String,
        @Path("repo-name") repoName: String
    ): Call<ArrayList<PullRequest>>
}