package br.com.itau.testepraticomobile.presentation.repositories

import br.com.itau.testepraticomobile.common.Constants
import br.com.itau.testepraticomobile.data.providers.GitHubProvider
import br.com.itau.testepraticomobile.domain.Repository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
class RepositoriesPresenter : RepositoriesContract.Presenter, KoinComponent {
    private val TAG: String = "CustomLog - ${RepositoriesPresenter::class.java.simpleName}"

    private lateinit var view: RepositoriesContract.View

    private var incompleteResults = true
    private var pageRepository = 1

    private val gitHubProvider: GitHubProvider by inject()
    private val listRepositoriesCallback = object : GitHubProvider.ExpectedListRepositories {
        override fun onSuccess(
            message: String,
            repositories: ArrayList<Repository>,
            incompleteResults: Boolean
        ) {
            if (this@RepositoriesPresenter.incompleteResults) {
                if (pageRepository == 1) {
                    view.initRepositories(repositories)
                } else {
                    view.includeRepositories(repositories)
                }

                pageRepository++
            } else {
                view.showFeedback("Você chegou no fim da página!\nNão há mais repositórios para buscar.")
            }

            this@RepositoriesPresenter.incompleteResults = incompleteResults
            view.hideLoading()
        }

        override fun onFailure(message: String) {
            view.showFeedback(message)
            view.hideLoading()
        }
    }

    override fun getRepositories() {
        view.showLoading()

        this.gitHubProvider.buscarRepositorios(
            this.listRepositoriesCallback,
            Constants.LANGUAGE_REPOSITORIES,
            Constants.SORT_REPOSITORIES,
            this.pageRepository
        )
    }

    override fun onAttach(view: RepositoriesContract.View) {
        this.view = view
    }

    override fun onDetach() {
    }
}