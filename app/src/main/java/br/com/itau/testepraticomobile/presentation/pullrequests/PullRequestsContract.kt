package br.com.itau.testepraticomobile.presentation.pullrequests

import br.com.itau.testepraticomobile.domain.PullRequest
import br.com.itau.testepraticomobile.domain.Repository
import br.com.itau.testepraticomobile.presentation.BasePresenter
import br.com.itau.testepraticomobile.presentation.BaseView

/**
 * Created by Denis Magno on 04/12/2020
 * denis_magno16@hotmail.com
 */
interface PullRequestsContract {
    interface Presenter : BasePresenter {
        fun onAttach(view: View, repository: Repository)

        fun getPullRequests()
    }

    interface View : BaseView<Presenter> {
        fun initPullRequests(pullRequests: ArrayList<PullRequest>)

        fun showLoading()

        fun hideLoading()

        fun showFeedback(message: String)
    }
}