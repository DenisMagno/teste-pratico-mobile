package br.com.itau.testepraticomobile.presentation.pullrequests.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.itau.testepraticomobile.databinding.ItemRecyclerPullRequestBinding
import br.com.itau.testepraticomobile.domain.PullRequest
import com.squareup.picasso.Picasso

/**
 * Created by Denis Magno on 04/12/2020
 * denis_magno16@hotmail.com
 */
class PullRequestsRecyclerAdapter(
    private val context: Context,
    private val pullRequests: ArrayList<PullRequest>
) : RecyclerView.Adapter<PullRequestsRecyclerAdapter.PullRequestViewHolder>() {
    private val TAG: String = "CustomLog - ${PullRequestsRecyclerAdapter::class.java.simpleName}"

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PullRequestViewHolder {
        val layoutInflater =
            this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding: ItemRecyclerPullRequestBinding =
            ItemRecyclerPullRequestBinding.inflate(layoutInflater, parent, false)

        return PullRequestViewHolder(binding.root, binding)
    }

    override fun onBindViewHolder(
        holder: PullRequestViewHolder,
        position: Int
    ) {
        holder.bindStatement(this.pullRequests[position])
    }

    override fun getItemCount(): Int {
        return this.pullRequests.size
    }

    override fun getItemId(position: Int): Long {
        return this.pullRequests[position].id
    }

    class PullRequestViewHolder(
        itemView: View,
        private val binding: ItemRecyclerPullRequestBinding
    ) : RecyclerView.ViewHolder(itemView) {
        private val TAG: String = "CustomLog - ${PullRequestViewHolder::class.java.simpleName}"

        fun bindStatement(pullRequest: PullRequest) {
            this.binding.txtTitle.text = pullRequest.title
            this.binding.txtBody.text = pullRequest.body
            this.binding.txtNameOwner.text = pullRequest.user.login
            this.binding.txtDate.text = pullRequest.created_at
                .replace("T", " ")
                .replace("Z", "")

            Picasso.get()
                .load(pullRequest.user.avatar_url)
                .resize(50, 50)
                .centerCrop()
                .into(this.binding.imgAvatarOwner)
        }
    }
}