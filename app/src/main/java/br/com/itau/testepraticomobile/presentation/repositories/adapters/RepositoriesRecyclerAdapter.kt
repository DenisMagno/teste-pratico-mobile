package br.com.itau.testepraticomobile.presentation.repositories.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.itau.testepraticomobile.databinding.ItemRecyclerRepositoryBinding
import br.com.itau.testepraticomobile.domain.Repository
import com.squareup.picasso.Picasso

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
class RepositoriesRecyclerAdapter(
    private val context: Context,
    private val repositories: ArrayList<Repository>,
    private val callback: ItemClickListener
) : RecyclerView.Adapter<RepositoriesRecyclerAdapter.RepositoryViewHolder>() {
    private val TAG: String = "CustomLog - ${RepositoriesRecyclerAdapter::class.java.simpleName}"

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RepositoryViewHolder {
        val layoutInflater =
            this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding: ItemRecyclerRepositoryBinding =
            ItemRecyclerRepositoryBinding.inflate(layoutInflater, parent, false)

        return RepositoryViewHolder(binding.root, binding, this.callback)
    }

    override fun onBindViewHolder(
        holder: RepositoryViewHolder,
        position: Int
    ) {
        holder.bindStatement(this.repositories[position])
    }

    override fun getItemCount(): Int {
        return this.repositories.size
    }

    override fun getItemId(position: Int): Long {
        return this.repositories[position].id
    }

    class RepositoryViewHolder(
        itemView: View,
        private val binding: ItemRecyclerRepositoryBinding,
        private val callback: ItemClickListener
    ) : RecyclerView.ViewHolder(itemView) {
        private val TAG: String = "CustomLog - ${RepositoryViewHolder::class.java.simpleName}"

        fun bindStatement(repository: Repository) {
            this.binding.txtName.text = repository.name
            this.binding.txtDescription.text = repository.description
            this.binding.txtStargazersCount.text = repository.stargazers_count.toString()
            this.binding.txtForksCount.text = repository.forks_count.toString()
            this.binding.txtNameOwner.text = repository.owner.login

            Picasso.get()
                .load(repository.owner.avatar_url)
                .resize(50, 50)
                .centerCrop()
                .into(this.binding.imgAvatarOwner)

            this.itemView.setOnClickListener { callback.onClick(repository) }
        }
    }

    interface ItemClickListener {
        fun onClick(repository: Repository)
    }
}