package br.com.itau.testepraticomobile.domain

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
data class PullRequest(
    val id: Long,
    val title: String,
    val body: String,
    val created_at: String,
    val user: User
) {
    override fun toString(): String {
        return "PullRequest(id=$id, title='$title', body='$body', created_at='$created_at', user=$user)"
    }
}