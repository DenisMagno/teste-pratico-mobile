package br.com.itau.testepraticomobile.common

/**
 * Created by Denis Magno on 04/12/2020
 * denis_magno16@hotmail.com
 */
internal object Constants {
    const val BUNDLE_REPOSITORY_EXTRA_REPOSITORIES_ACTIVITY_TO_PULL_REQUEST_ACTIVITY =
        "BUNDLE_REPOSITORY_EXTRA_REPOSITORIES_ACTIVITY_TO_PULL_REQUEST_ACTIVITY"

    const val LANGUAGE_REPOSITORIES = "language:Java"
    const val SORT_REPOSITORIES = "stars"
}