package br.com.itau.testepraticomobile.domain

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
data class Repository(
    val id: Long,
    val name: String,
    val description: String,
    val stargazers_count: Int,
    val forks_count: Int,
    val owner: Owner
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readInt(),
        parcel.readParcelable(Owner::class.java.classLoader)!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeInt(stargazers_count)
        parcel.writeInt(forks_count)
        parcel.writeParcelable(owner, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "Repository(id=$id, name='$name', description='$description', stargazers_count=$stargazers_count, forks_count=$forks_count, owner=$owner)"
    }

    companion object CREATOR : Parcelable.Creator<Repository> {
        override fun createFromParcel(parcel: Parcel): Repository {
            return Repository(parcel)
        }

        override fun newArray(size: Int): Array<Repository?> {
            return arrayOfNulls(size)
        }
    }
}