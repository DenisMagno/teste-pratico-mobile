package br.com.itau.testepraticomobile.domain

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
data class User(val id: Long, val login: String, val avatar_url: String) {
    override fun toString(): String {
        return "User(id=$id, login='$login', avatar_url='$avatar_url')"
    }
}