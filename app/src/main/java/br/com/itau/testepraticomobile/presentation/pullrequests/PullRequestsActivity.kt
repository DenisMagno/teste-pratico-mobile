package br.com.itau.testepraticomobile.presentation.pullrequests

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.itau.testepraticomobile.R
import br.com.itau.testepraticomobile.common.Constants
import br.com.itau.testepraticomobile.databinding.ActivityPullRequestsBinding
import br.com.itau.testepraticomobile.domain.PullRequest
import br.com.itau.testepraticomobile.domain.Repository
import br.com.itau.testepraticomobile.presentation.pullrequests.adapters.PullRequestsRecyclerAdapter
import com.google.android.material.snackbar.Snackbar
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * Created by Denis Magno on 04/12/2020
 * denis_magno16@hotmail.com
 */
class PullRequestsActivity : AppCompatActivity(), PullRequestsContract.View, KoinComponent {
    private val TAG: String = "CustomLog - ${PullRequestsActivity::class.java.simpleName}"

    private val presenter: PullRequestsPresenter by inject()

    private lateinit var binding: ActivityPullRequestsBinding

    private lateinit var pullRequestsRecyclerAdapter: PullRequestsRecyclerAdapter
    private val pullRequests = ArrayList<PullRequest>()

    private lateinit var loadingDialog: Dialog
    private var isLoading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.binding = ActivityPullRequestsBinding.inflate(this.layoutInflater)

        val repository =
            this.intent.getParcelableExtra<Repository>(Constants.BUNDLE_REPOSITORY_EXTRA_REPOSITORIES_ACTIVITY_TO_PULL_REQUEST_ACTIVITY)

        this.presenter.onAttach(this, repository!!)

        this.initTitleAppBar(repository.name)

        setContentView(this.binding.root)
    }

    override fun onStart() {
        super.onStart()

        this.configRecyclerView()
        this.configLoadingDialog()
    }

    override fun onResume() {
        super.onResume()

        this.presenter.getPullRequests()
    }

    override fun onDestroy() {
        super.onDestroy()

        this.presenter.onDetach()
    }

    private fun configRecyclerView() {
        val lltManager = LinearLayoutManager(this)
        this.binding.rcvPullRequests.layoutManager = lltManager

        val dividerItemDecoration = DividerItemDecoration(this, lltManager.orientation)
        dividerItemDecoration.setDrawable(
            this.resources.getDrawable(R.drawable.shape_divider_gray)
        )

        this.binding.rcvPullRequests.addItemDecoration(dividerItemDecoration)

        this.pullRequestsRecyclerAdapter =
            PullRequestsRecyclerAdapter(this, this.pullRequests)
        this.binding.rcvPullRequests.adapter = this.pullRequestsRecyclerAdapter
    }

    private fun configLoadingDialog() {
        this.loadingDialog = Dialog(this, android.R.style.Theme_Black)

        val view: View =
            LayoutInflater.from(this).inflate(R.layout.view_loading, null as ViewGroup?)

        this.loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.loadingDialog.window?.setBackgroundDrawableResource(R.color.grayTransparent)
        this.loadingDialog.setContentView(view)
        this.loadingDialog.setCancelable(false)
    }

    private fun initTitleAppBar(repositoryName: String) {
        this.supportActionBar?.title = String.format(
            resources.getString(R.string.activity_pull_requests_label_text),
            repositoryName
        )
    }

    override fun initPullRequests(pullRequests: ArrayList<PullRequest>) {
        this.pullRequests.clear()

        this.pullRequests.addAll(pullRequests)

        this.pullRequestsRecyclerAdapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        if (!this.isLoading) {
            this.loadingDialog.show()
            this.isLoading = true
        }
    }

    override fun hideLoading() {
        if (this.isLoading) {
            this.loadingDialog.dismiss()
            this.isLoading = false
        }
    }

    override fun showFeedback(message: String) {
        Snackbar.make(this.binding.root, message, Snackbar.LENGTH_SHORT).show()
    }
}