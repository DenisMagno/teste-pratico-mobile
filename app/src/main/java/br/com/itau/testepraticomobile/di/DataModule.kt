package br.com.itau.testepraticomobile.di

import br.com.itau.testepraticomobile.data.RetrofitInitializer
import br.com.itau.testepraticomobile.data.providers.GitHubProvider
import org.koin.dsl.module

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
fun injectDataModule() = module {
    factory { RetrofitInitializer() }
    single { GitHubProvider() }
}