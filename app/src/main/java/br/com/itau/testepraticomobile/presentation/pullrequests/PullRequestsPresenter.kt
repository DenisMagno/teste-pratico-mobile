package br.com.itau.testepraticomobile.presentation.pullrequests

import br.com.itau.testepraticomobile.data.providers.GitHubProvider
import br.com.itau.testepraticomobile.domain.PullRequest
import br.com.itau.testepraticomobile.domain.Repository
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * Created by Denis Magno on 04/12/2020
 * denis_magno16@hotmail.com
 */
class PullRequestsPresenter : PullRequestsContract.Presenter, KoinComponent {
    private val TAG: String = "CustomLog - ${PullRequestsPresenter::class.java.simpleName}"

    private lateinit var view: PullRequestsContract.View
    private lateinit var repository: Repository

    private val gitHubProvider: GitHubProvider by inject()
    private val listPullRequestsCallback = object : GitHubProvider.ExpectedListPullRequests {
        override fun onSuccess(message: String, pullRequests: ArrayList<PullRequest>) {
            view.initPullRequests(pullRequests)
            view.hideLoading()
        }

        override fun onFailure(message: String) {
            view.showFeedback(message)
            view.hideLoading()
        }
    }

    override fun onAttach(view: PullRequestsContract.View, repository: Repository) {
        this.view = view
        this.repository = repository
    }

    override fun getPullRequests() {
        this.view.showLoading()

        this.gitHubProvider.buscarPullRequests(
            this.listPullRequestsCallback,
            this.repository.owner.login,
            this.repository.name
        )
    }

    override fun onDetach() {
    }
}