package br.com.itau.testepraticomobile.data.providers

import android.util.Log
import br.com.itau.testepraticomobile.data.providers.generics.BaseProvider
import br.com.itau.testepraticomobile.data.resources.GitHubResource
import br.com.itau.testepraticomobile.data.responses.BaseResponse
import br.com.itau.testepraticomobile.domain.PullRequest
import br.com.itau.testepraticomobile.domain.Repository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
class GitHubProvider : BaseProvider() {
    private val resource: GitHubResource = this.retrofit.getGitHubResource()

    fun buscarRepositorios(
        callback: ExpectedListRepositories,
        language: String,
        sort: String,
        page: Int
    ) {
        val callBuscarRepositorios: Call<BaseResponse<Repository>> =
            this.resource.listRepositories(language, sort, page)
        callBuscarRepositorios.enqueue(object : Callback<BaseResponse<Repository>> {
            override fun onResponse(
                call: Call<BaseResponse<Repository>>,
                response: Response<BaseResponse<Repository>>
            ) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        callback.onSuccess(
                            "Busca efetuada com sucesso.",
                            response.body()!!.items,
                            response.body()!!.incomplete_results
                        )
                    } else {
                        Log.e(TAG, "Http error: Corpo da mensagem é nulo")
                        callback.onFailure("Corpo da mensagem é nulo")
                    }
                } else {
                    Log.e(TAG, "Http error: ${response.message()}")
                    callback.onFailure(response.message())
                }
            }

            override fun onFailure(call: Call<BaseResponse<Repository>>, t: Throwable) {
                if (call.isCanceled) {
                    Log.e(TAG, "buscarRepositorios: Requisição cancelada.")
                } else {
                    Log.e(TAG, "Error: ${t.message!!}")
                    callback.onFailure(t.message!!)
                }
            }
        })
    }

    fun buscarPullRequests(
        callback: ExpectedListPullRequests,
        ownerName: String,
        repoName: String
    ) {
        val callBuscarPullRequests: Call<ArrayList<PullRequest>> =
            this.resource.listPullRequests(ownerName, repoName)
        callBuscarPullRequests.enqueue(object : Callback<ArrayList<PullRequest>> {
            override fun onResponse(
                call: Call<ArrayList<PullRequest>>,
                response: Response<ArrayList<PullRequest>>
            ) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        callback.onSuccess("Busca efetuada com sucesso.", response.body()!!)
                    } else {
                        Log.e(TAG, "Http error: Corpo da mensagem é nulo")
                        callback.onFailure("Corpo da mensagem é nulo")
                    }
                } else {
                    Log.e(TAG, "Http error: ${response.message()}")
                    callback.onFailure(response.message())
                }
            }

            override fun onFailure(call: Call<ArrayList<PullRequest>>, t: Throwable) {
                if (call.isCanceled) {
                    Log.e(TAG, "buscarPullRequests: Requisição cancelada.")
                } else {
                    Log.e(TAG, "Error: ${t.message!!}")
                    callback.onFailure(t.message!!)
                }
            }
        })
    }

    interface ExpectedListRepositories {
        fun onSuccess(
            message: String,
            repositories: ArrayList<Repository>,
            incompleteResults: Boolean
        )

        fun onFailure(message: String)
    }

    interface ExpectedListPullRequests {
        fun onSuccess(message: String, pullRequests: ArrayList<PullRequest>)

        fun onFailure(message: String)
    }
}