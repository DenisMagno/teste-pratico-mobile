package br.com.itau.testepraticomobile

import android.app.Application
import android.util.Log
import br.com.itau.testepraticomobile.di.injectDataModule
import br.com.itau.testepraticomobile.di.injectPresentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
class App : Application() {
    private val TAG: String = "CustomLog - ${App::class.java.simpleName}"

    override fun onCreate() {
        super.onCreate()
        Log.i(TAG, "Application started!")

        startKoin {
            androidContext(this@App)
            modules(
                arrayListOf(
                    injectPresentationModule(),
                    injectDataModule()
                )
            )
        }
    }
}