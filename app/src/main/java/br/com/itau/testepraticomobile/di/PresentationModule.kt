package br.com.itau.testepraticomobile.di

import br.com.itau.testepraticomobile.presentation.repositories.RepositoriesPresenter
import br.com.itau.testepraticomobile.presentation.pullrequests.PullRequestsPresenter
import org.koin.dsl.module

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
fun injectPresentationModule() = module {
    factory { RepositoriesPresenter() }
    factory { PullRequestsPresenter() }
}