package br.com.itau.testepraticomobile.domain

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
data class Owner(val id: Long, val login: String, val avatar_url: String) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString()!!,
        parcel.readString()!!
    )

    override fun toString(): String {
        return "Owner(id=$id, login='$login', avatar_url='$avatar_url')"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(login)
        parcel.writeString(avatar_url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Owner> {
        override fun createFromParcel(parcel: Parcel): Owner {
            return Owner(parcel)
        }

        override fun newArray(size: Int): Array<Owner?> {
            return arrayOfNulls(size)
        }
    }
}