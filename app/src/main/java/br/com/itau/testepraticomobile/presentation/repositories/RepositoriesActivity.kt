package br.com.itau.testepraticomobile.presentation.repositories

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.itau.testepraticomobile.R
import br.com.itau.testepraticomobile.common.Constants
import br.com.itau.testepraticomobile.databinding.ActivityRepositoriesBinding
import br.com.itau.testepraticomobile.domain.Repository
import br.com.itau.testepraticomobile.presentation.repositories.adapters.RepositoriesRecyclerAdapter
import br.com.itau.testepraticomobile.presentation.pullrequests.PullRequestsActivity
import com.google.android.material.snackbar.Snackbar
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
class RepositoriesActivity : AppCompatActivity(), RepositoriesContract.View, KoinComponent {
    private val TAG: String = "CustomLog - ${RepositoriesActivity::class.java.simpleName}"

    private val presenter: RepositoriesPresenter by inject()

    private lateinit var binding: ActivityRepositoriesBinding

    private lateinit var repositoriesRecyclerAdapter: RepositoriesRecyclerAdapter
    private lateinit var callbackClickItemRepository: RepositoriesRecyclerAdapter.ItemClickListener
    private val repositories = ArrayList<Repository>()

    private lateinit var loadingDialog: Dialog
    private var isLoading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.binding = ActivityRepositoriesBinding.inflate(this.layoutInflater)

        this.presenter.onAttach(this)

        setContentView(this.binding.root)
    }

    override fun onStart() {
        super.onStart()

        this.initEvents()
        this.configRecyclerView()
        this.configLoadingDialog()
    }

    override fun onResume() {
        super.onResume()

        this.presenter.getRepositories()
    }

    override fun onDestroy() {
        super.onDestroy()

        this.presenter.onDetach()
    }

    private fun initEvents() {
        this.callbackClickItemRepository = object : RepositoriesRecyclerAdapter.ItemClickListener {
            override fun onClick(repository: Repository) {
                navigateToPullRequests(repository)
            }
        }
    }

    private fun configRecyclerView() {
        val lltManager = LinearLayoutManager(this)
        this.binding.rcvRepositories.layoutManager = lltManager

        this.binding.rcvRepositories.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (!recyclerView.canScrollVertically(1) && RecyclerView.SCROLL_STATE_IDLE == newState) {
                    presenter.getRepositories()
                }
            }
        })

        val dividerItemDecoration = DividerItemDecoration(this, lltManager.orientation)
        dividerItemDecoration.setDrawable(
            this.resources.getDrawable(R.drawable.shape_divider_gray)
        )

        this.binding.rcvRepositories.addItemDecoration(dividerItemDecoration)

        this.repositoriesRecyclerAdapter =
            RepositoriesRecyclerAdapter(
                this,
                this.repositories,
                this.callbackClickItemRepository
            )
        this.binding.rcvRepositories.adapter = this.repositoriesRecyclerAdapter
    }

    private fun configLoadingDialog() {
        this.loadingDialog = Dialog(this, android.R.style.Theme_Black)

        val view: View =
            LayoutInflater.from(this).inflate(R.layout.view_loading, null as ViewGroup?)

        this.loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        this.loadingDialog.window?.setBackgroundDrawableResource(R.color.grayTransparent)
        this.loadingDialog.setContentView(view)
        this.loadingDialog.setCancelable(false)
    }

    private fun navigateToPullRequests(repository: Repository) {
        val intent = Intent(this@RepositoriesActivity, PullRequestsActivity::class.java)
        intent.putExtra(
            Constants.BUNDLE_REPOSITORY_EXTRA_REPOSITORIES_ACTIVITY_TO_PULL_REQUEST_ACTIVITY,
            repository
        )
        this.startActivity(intent)
    }

    override fun initRepositories(repositories: ArrayList<Repository>) {
        this.repositories.clear()

        this.repositories.addAll(repositories)

        this.repositoriesRecyclerAdapter.notifyDataSetChanged()
    }

    override fun includeRepositories(repositories: ArrayList<Repository>) {
        this.repositories.addAll(repositories)

        this.repositoriesRecyclerAdapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        if (!this.isLoading) {
            this.loadingDialog.show()
            this.isLoading = true
        }
    }

    override fun hideLoading() {
        if (this.isLoading) {
            this.loadingDialog.dismiss()
            this.isLoading = false
        }
    }

    override fun showFeedback(message: String) {
        Snackbar
            .make(this.binding.root, message, Snackbar.LENGTH_LONG)
            .show()
    }
}
