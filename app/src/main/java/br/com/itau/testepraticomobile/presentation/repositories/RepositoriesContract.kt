package br.com.itau.testepraticomobile.presentation.repositories

import br.com.itau.testepraticomobile.domain.Repository
import br.com.itau.testepraticomobile.presentation.BasePresenter
import br.com.itau.testepraticomobile.presentation.BaseView

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
interface RepositoriesContract {
    interface Presenter : BasePresenter {
        fun onAttach(view: View)

        fun getRepositories()
    }

    interface View : BaseView<Presenter> {
        fun initRepositories(repositories: ArrayList<Repository>)

        fun includeRepositories(repositories: ArrayList<Repository>)

        fun showLoading()

        fun hideLoading()

        fun showFeedback(message: String)
    }
}