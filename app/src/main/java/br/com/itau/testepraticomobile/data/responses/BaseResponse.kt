package br.com.itau.testepraticomobile.data.responses

/**
 * Created by Denis Magno on 03/12/2020
 * denis_magno16@hotmail.com
 */
class BaseResponse<T>(
    val total_count: Int,
    val incomplete_results: Boolean,
    val items: ArrayList<T>
) {
    override fun toString(): String {
        return "BaseResponse(total_count=$total_count, incomplete_results=$incomplete_results, items=$items)"
    }
}